//Mishal Alarifi 1942365
public class Bicycle {
	private int numberGears;
	private String manufacturer;
	private double maxSpeed;
	
	public Bicycle(int newNumberGears, String newManufacturer, double newMaxSpeed){
		numberGears = newNumberGears;
		manufacturer = newManufacturer;
		maxSpeed = newMaxSpeed;
	}
	
	public int getNumberGears(){
		return numberGears;
	}
	public String getManufacturer(){
		return manufacturer;
	}
	public double getMaxSpeed(){
		return maxSpeed;
	}
	
	public String toString(){
		return "Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", MaxSpeed: " + maxSpeed;
	}
}
